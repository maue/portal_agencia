jQuery(function($){
    'use strict';

    // -------------------------------------------------------------
    //   Basic Navigation
    // -------------------------------------------------------------
    

(function () {
var $frame = $('#basic1');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic2');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic3');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic4');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic5');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic6');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic7');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic8');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic9');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic10');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic11');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic12');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic13');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic14');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic15');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic16');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic17');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic18');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic19');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic20');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic21');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic22');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic23');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic24');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic25');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic26');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic27');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic28');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic29');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic30');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic31');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic32');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic33');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic34');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic35');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic36');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic37');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic38');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic39');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic40');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic41');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic42');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic43');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic44');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic45');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic46');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic47');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic48');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic49');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic50');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic51');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic52');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic53');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic54');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic55');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic56');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic57');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic58');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic59');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic60');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic61');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic62');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic63');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic64');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic65');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic66');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic67');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic68');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic69');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic70');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic71');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic72');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic73');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic74');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic75');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic76');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic77');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic78');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic79');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic80');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic81');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic82');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic83');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic84');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic85');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic86');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic87');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic88');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic89');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic90');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic91');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic92');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic93');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic94');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic95');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic96');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic97');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic98');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());




(function () {
var $frame = $('#basic99');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
horizontal: 1,
itemNav: 'centered',
smart: 1,
activateOn: 'click',
mouseDragging: 1,
touchDragging: 1,
releaseSwing: 1,
startAt: 1,
scrollBar: $wrap.find('.scrollbar'),
scrollBy: 1,
speed: 300,
elasticBounds: 1,
easing: 'easeOutExpo',
dragHandle: 1,
dynamicHandle: 1,
clickBar: 1,

// Buttons
prev: $wrap.find('.prev'),
next: $wrap.find('.next'),

prevPage: $wrap.find('.prevPage'),
nextPage: $wrap.find('.nextPage')
});
}());


});
