<?php

class galeria_galerias {
    
    
    public function render() {
        
        $c = EASYNC__model_conn::get_conn();
        $q = "SELECT distinct YEAR(data) ano FROM galeria_galeria WHERE status = 1 order by year(data) desc";
        $r = $c->qcv($q, "ano");
        $ano_exibir = $r[0];
        
        if(util::GET_isset('ano')) {
            $ano_exibir = util::GET('ano');
        }
        
        
        echo '<div style="position: relative; top: -70px;">
<table width="770" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td valign="top" nowrap="nowrap">
		<div style="margin-top: 80px; margin-right:30px;">
		';
        
        foreach($r as $v) {
            echo '<div><a class="voltar" href="?pagina=galerias&ano='.$v.'">'.$v.'</a></div><br /><br />';
        }
        
        echo '</div>
		</td>
		<td width="545" valign="top">
		<div style="margin-top: 20px;">
			<div style="font-size: 16px; color: #333; font-weight: bold; margin-bottom:20px;">Selecione a galeria (ano
				'.$ano_exibir.'):</div>';
        
        $q = "SELECT id, nome FROM galeria_galeria WHERE YEAR(data) = $ano_exibir AND status = 1";
        $r = $c->qcv($q, "id,nome");
        
        
        if($r != null) {
            foreach($r as $v) {
                $id_galeria = $v[0];
                $nome = $v[1];
                
                
                
                $q = "SELECT id FROM galeria_foto WHERE fk_galeria = " . $id_galeria . " LIMIT 1";
                $c = EASYNC__model_conn::get_conn();
                $r = $c->qcv($q, "id");
                
                
                $foto = '(Galeria sem imagens)';
                if($r != null) {
                    $foto = EASYNC__galeria_foto::getByPK($r[0]);
                    
                    $arq = $foto->getArquivo();
                    $foto = '<img src="../novo/arquivo_galeria/miniatura/'.$arq.'" />';
                }
                
                echo '
				<div class="imagem_galeria" align="center">
					<a href="?pagina=galeria&id='.$id_galeria.'" >'.$foto.'</a>
					<div class="descricao_foto" align="center"><a href="?pagina=galeria&id='.$id_galeria.'" >Galeria: <b>' . $nome . '</b></a></div>
					    
				</div>
					    
				';
            }
        }else{
            echo 'Nenhuma galeria encontrada.';
        }
        
        echo '
            
		</div>
		</td>
	</tr>
</table>
</div>
		';
    }
    
}

?>