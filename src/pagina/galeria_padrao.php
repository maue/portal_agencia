<?

class galeria_padrao {

    function imprime($id) {


        $str = "

(function () {
    var \$frame = $('#basic$id');
    var \$wrap  = \$frame.parent();

    // Call Sly on frame
    \$frame.sly({
        horizontal: 1,
        itemNav: 'centered',
        smart: 1,
        activateOn: 'click',
        mouseDragging: 1,
        touchDragging: 1,
        releaseSwing: 1,
        startAt: 1,
        scrollBar: \$wrap.find('.scrollbar'),
        scrollBy: 1,
        speed: 300,
        elasticBounds: 1,
        easing: 'easeOutExpo',
        dragHandle: 1,
        dynamicHandle: 1,
        clickBar: 1,

        // Buttons
        prev: \$wrap.find('.prev'),
        next: \$wrap.find('.next'),

        prevPage: \$wrap.find('.prevPage'),
        nextPage: \$wrap.find('.nextPage')
    });
}());


";
        return $str;
    }
    function render() {


        $horizontalJS = "
        jQuery(function($){
            'use strict';
        
            // -------------------------------------------------------------
            //   Basic Navigation
            // -------------------------------------------------------------
            ";
        for($i=1; $i<100; $i++) {
            $horizontalJS.= $this->imprime($i);
        }
        $horizontalJS.= "    });";
        // echo $horizontalJS;


        $largura = '515px';
        ?>


        <style>
            .imagem_galeria {
                display: inline-block;
                border: 1px solid #fff;
                background-color:#fff;
                margin: 6px;
                padding: 3px;
            }
            .imagem_galeria a {
                color: #333;
            }
            .imagem_galeria:hover {
                border-color: #00BFA8;
                background-color:#E8E8E8;
            }

            .overlay {
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                background-color: #000;
                filter:alpha(opacity=50);
                -moz-opacity:0.8;
                -khtml-opacity: 0.8;
                opacity: 0.8;
                z-index: 10000;
            }

            .voltar_estrutura {
                color:#fff;
            }

            .restrito {
                color: #fff;
            }


        </style>

        <div style="margin-bottom:0px; ">
            <div style=" position: relative; z-index: 9000; font-size:13px; font-family:Verdana, Geneva, sans-serif; ">

                <!-- lateral direita cinza: -->

                <div style="background-color:#828D6B; color:#fff; position:relative; height:; padding-left:100px; padding-right:100px;" class="bloco_modalidade">
                     <?php


                    $c = EASYNC5__model_conn::get_conn();

                    $q = "SELECT distinct YEAR(data) ano FROM galeria_galeria WHERE id > 0 AND (status = 1) ORDER BY data";
                    $r_anos = $c->qcv($q, "ano");


                     $ano = util::GET('ano');
                     if($ano == '') {
                         $ano = $r_anos[sizeof($r_anos) - 1];
                     }

                    ?>

                    <div class="titulo_modalidade">Galeria de fotos - Ano <? echo $ano; ?></div>


                    <div id="overlay">&nbsp;</div>

                    <?php


                    echo '<div style="margin-bottom: 20px;">';
                    foreach ($r_anos as $v) {
                        echo '<a class="ano_galeria" href="?acao=galeria_padrao&ano='.$v.'">' . $v . '</a>';
                    }
                    echo '</div>';

                    $q = "

  SELECT id 
  FROM galeria_galeria g
  WHERE 
  YEAR(data) = $ano 
  AND id > 0 
  AND (status = 1)
  AND (SELECT COUNT(*) FROM galeria_foto f WHERE f.fk_galeria = g.id) > 0
  ORDER BY data
  
  ";
                    $r = $c->qcv($q, "id");
                    $i = 1;

                    $login = '';
                    $login.= modelo__sessao::get_login_usuario_logado();
                    $login.= modelo__sessao_pai::get_login_usuario_logado();
                    $lsisaula = modelo__sessao::get_login_usuario_logado();
                    $lsispai = modelo__sessao_pai::get_login_usuario_logado();
//                    echo "SISAULA>: $lsisaula";
//                    echo "SISPAI>: $lsispai";
//
//                    echo "LOGIN>: $login";

                    if($login != '') {
                        foreach ($r as $v) {
                            $g = new galeria();
                            $g->render($v, "basic$i");
                            $i++;
                        }
                    }else{


                        $localh = $_SERVER['SERVER_NAME'] == 'localhost';
                        $url_pai = 'http://www.colegioportalcuiaba.com.br/novo/sispai/index.php?acao=login';
                        if($localh) {
                            $url_pai = '../../portal/sispai/?acao=login';
                        }

                        echo 'Acesso restrito a Pais. Favor fazer login no <b><a class="restrito" href="'.$url_pai.'">SISAULA</a></b>.<br/><br/><br/>';
                    }


                    ?>


                    <!-- Scripts -->
                    <script src="js/vendor/plugins.js"></script>
                    <script src="js/sly.min.js"></script>
                    <script src="js/horizontal.js"></script>



                    <script>


                        $(function(){

                            $('#overlay').hide();
                            $('#overlay').addClass('overlay');
                            var $gallery = $('a.imagem_item').simpleLightbox(
                                // {'navText': ['VOLTAR','AVANÃ‡AR']}
                            );



                            $gallery.on('show.simplelightbox', function(){
                                console.log('Requested for showing');
                                $('#overlay').fadeIn(200);
                            })
                                .on('shown.simplelightbox', function(){
                                    console.log('Shown');
                                })
                                .on('close.simplelightbox', function(){
                                    console.log('Requested for closing');
                                })
                                .on('closed.simplelightbox', function(){
                                    console.log('Closed');
                                    $('#overlay').fadeOut(200);
                                })
                        });
                    </script>


                </div>
            </div>
        </div>
        <?
    }
}


?>
