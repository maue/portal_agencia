<?php
/**
 * Created by PhpStorm.
 * User: marcel
 * Date: 31/08/2019
 * Time: 11:42
 */

class galeria {


    private $link_galerias = "?acao=estrua";
    private $sly_horizontal_id = "";

    public function __construct() {

    }

    public function render($galeria_id, $sly_horizontal_id)
    {
        $this->sly_horizontal_id = $sly_horizontal_id;
        $this->exibe_fotos($galeria_id);

        echo '&nbsp;';
    }

    private function exibe_fotos($galeria_id)
    {


        $galeria = EASYNC5__galeria_galeria::getByPK($galeria_id);


        $find_fotos = new EASYNC5___FIND__galeria_foto();
        $find_fotos->filterByFk_galeria($galeria_id);
        $find_fotos->orderBy(EASYNC5__galeria_foto::$COLUMN_nome);
        $colecao = $find_fotos->get();



        echo '
        <div style="font-size: 20px; color: #fff;">'.$galeria->getNome().'</div>
        


    
        <!-- INÃCIO HORIZONTAL -->
        <div>
            <div class="pagespan " >
                <div class="wrap" >

                    <div class="scrollbar">
                        <div class="handle">
                            <div class="mousearea"></div>
                        </div>
                    </div>

                        <div style="position:absolute; top:30px; left:-70px;">
                            
                                <img class="btn prevPage" src="img/estrutura/esq.png" style="padding:20px;" />
                        </div>
                        <div class="frame" id="'.$this->sly_horizontal_id.'">
                            <ul class="clearfix">
    ';
        // <li>0</li><li>1</li><li>2</li><li>3</li><li>4</li><li>5</li><li>6</li><li>7</li><li>8</li><li>9</li>
        // <li>10</li><li>11</li><li>12</li>
        // echo '<li>0</li><li>1</li><li>2</li><li>3</li><li>4</li><li>5</li><li>6</li><li>7</li><li>8</li><li>9</li>                        <li>10</li><li>11</li><li>12</li>';

        if($colecao->getAmount() > 0 && 1==1)
        {
            for($i=0; $i<$colecao->getAmount(); $i++)
            {
                $foto = $colecao->getByIndex($i);
                $nome = $foto->getNome();
                $arq = $foto->getArquivo();


                $localh = $_SERVER['SERVER_NAME'] == 'localhost';
                $relativo = '../novo/';
                if($localh) {
                    $relativo = '../../portal/';

                }

                echo '<li>
						
                <a class="imagem_item" href="'.$relativo.'arquivo_galeria/'.$arq.'" data-lightbox="roadtrip" title="'.$nome.'">
                    <img src="'.$relativo.'arquivo_galeria/miniatura/'.$arq.'" />
                </a>
				</li>';
            }
        }else{
            echo '<div style="padding:20px;" align="center">Nenhuma foto encontrada nesta galeria. <br /><br /><a class="link_voltar" href="'.$this->link_galerias.'">&lt;&lt; Voltar para Galerias</a></div>';
        }


        echo '                  
                    </ul>
                </div>
                <div style="position:absolute; top:30px; right:-70px;">
                    <img class="btn nextPage" src="img/estrutura/dir.png" style="padding:20px;" />
                </div>
                <ul class="pages"></ul>
            

            <!--
            <div class="controls center">
                    <button class="btn prev">LEFT item</button>
                    <button class="btn next">LEFT item</button>

            </div>
            -->
            </div>

            </div>

            </div>
            <!-- FIM HORIZONTAL -->

';
    }

}


?>