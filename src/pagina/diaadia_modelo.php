<?

class diaadia_modelo {

    private $bg_color;
    private $img_slice;
    private $min_width;
    private $nome_pagina;
    private $img_rodape;
    private $img_aditional_params;

    public function __construct(
            $bg_color = '#FE8222',
            $img_slice = 'slice_r1_c1.jpg',
            $min_width = '150',
            $nome_pagina = 'PROJETOS',
            $img_rodape = 'projetos_retangular.jpg',
            $img_aditional_params = null
    )
    {
        $this->bg_color = $bg_color;
        $this->img_slice = $img_slice;
        $this->min_width = $min_width;
        $this->nome_pagina = $nome_pagina;
        $this->img_rodape = $img_rodape;
        $this->img_aditional_params = $img_aditional_params !== null ? $img_aditional_params : '';
    }

    function render() {
        $largura = '515px';
        ?>


        <div style="margin-bottom:0px; background-color: <?=$this->bg_color; ?>">

            <div style="position: relative; ">

                <div class="helvetica1" style="position: relative; font-size: 19px; margin-left:150px; margin-right: 150px; padding-top:46px; text-align: justify">

                    <div style="position: absolute; top:20px;">

                        <img 
                        src="slices/<?=$this->img_slice; ?>" 
                        style="min-width: <?=$this->min_width; ?>px; <?=$this->img_aditional_params; ?>" />
                        
                        </div>
                    <div style="margin-left: 200px; min-height: 100px;">
                        <?php

                        $array_id = config_site::get_id_paginas_inicio();
                        $id_pagina = $array_id[$this->nome_pagina];

                        $pagina = EASYNC5__site_inicio::getByPK($id_pagina);
                        $texto = $pagina->getTexto_mobile()->hasValue() ? $pagina->getTexto_mobile()->value() : '';
                        echo $texto;

                        ?>
                    </div>

                </div>

            </div>

            <div style=" position: relative; z-index: 9000; font-size:13px; font-family:Verdana, Geneva, sans-serif; ">

                <img src="img/<?=$this->img_rodape; ?>?rand=125" />
            </div>

            <script>
                $( document ).ready(function() {

                });

            </script>
        </div>

        <?
    }
}
?>