<?

class estrutura_galeria {

    function imprime($id) {


        $str = "

(function () {
    var \$frame = $('#basic$id');
    var \$wrap  = \$frame.parent();

    // Call Sly on frame
    \$frame.sly({
        horizontal: 1,
        itemNav: 'centered',
        smart: 1,
        activateOn: 'click',
        mouseDragging: 1,
        touchDragging: 1,
        releaseSwing: 1,
        startAt: 1,
        scrollBar: \$wrap.find('.scrollbar'),
        scrollBy: 1,
        speed: 300,
        elasticBounds: 1,
        easing: 'easeOutExpo',
        dragHandle: 1,
        dynamicHandle: 1,
        clickBar: 1,

        // Buttons
        prev: \$wrap.find('.prev'),
        next: \$wrap.find('.next'),

        prevPage: \$wrap.find('.prevPage'),
        nextPage: \$wrap.find('.nextPage')
    });
}());


";
        return $str;
    }
    function render() {

        /*
        for($i=1; $i<100; $i++) {
            echo $this->imprime($i);
        }
        */


        $largura = '515px';
        ?>


        <style>
            .imagem_galeria {
                display: inline-block;
                border: 1px solid #fff;
                background-color:#fff;
                margin: 6px;
                padding:3px;
            }
            .imagem_galeria a {
                color: #333;
            }
            .imagem_galeria:hover {
                border-color: #00BFA8;
                background-color:#E8E8E8;
            }

            .overlay {
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                background-color: #000;
                filter:alpha(opacity=50);
                -moz-opacity:0.8;
                -khtml-opacity: 0.8;
                opacity: 0.8;
                z-index: 10000;
            }

            .voltar_estrutura {
                color:#fff;
            }


        </style>

        <div style="margin-bottom:0px; ">
            <div style=" position: relative; z-index: 9000; font-size:13px; font-family:Verdana, Geneva, sans-serif; ">

                <!-- lateral direita cinza: -->

                <div style="background-color:#828D6B; color:#fff; position:relative; height:; padding-left:100px; padding-right:100px;" class="bloco_modalidade">
                    <div class="titulo_modalidade">CONHEÇA NOSSA ESTRUTURA</div>


                    <div id="overlay">&nbsp;</div>

                    <?php


                    $c = EASYNC5__model_conn::get_conn();
                    $q = "SELECT 
            (
                SELECT id 
                FROM galeria_galeria 
                WHERE 
                    nome = 'Salas especiais' AND 
                    status = 3
            ) AS id_salas_especiais,
            (
                SELECT id 
                FROM galeria_galeria 
                WHERE 
                    nome = 'Recreação' AND 
                    status = 3
            ) AS id_recreacao,
            (
                SELECT id 
                FROM galeria_galeria 
                WHERE 
                    nome = 'Áreas Internas e Externas' AND 
                    status = 3
            ) AS id_areas_internas
            
            ";
                    $r = $c->qcv($q, "id_salas_especiais,id_recreacao,id_areas_internas");
                    $id_salas_especiais = (int)$r[0][0];
                    $id_recreacao = (int)$r[0][1];
                    $id_areas_internas = (int)$r[0][2];


                    $g = new galeria();
                    $g->render($id_salas_especiais, "basic1");

                    $g = new galeria();
                    $g->render($id_recreacao, "basic2");

                    $g = new galeria();
                    $g->render($id_areas_internas, "basic3");

                    ?>


                    <!-- Scripts -->
                    <script src="js/vendor/plugins.js"></script>
                    <script src="js/sly.min.js"></script>
                    <script src="js/horizontal.js"></script>



                    <script>


                        $(function(){

                            $('#overlay').hide();
                            $('#overlay').addClass('overlay');
                            var $gallery = $('a.imagem_item').simpleLightbox(
                                // {'navText': ['VOLTAR','AVANÃ‡AR']}
                            );



                            $gallery.on('show.simplelightbox', function(){
                                console.log('Requested for showing');
                                $('#overlay').fadeIn(200);
                            })
                                .on('shown.simplelightbox', function(){
                                    console.log('Shown');
                                })
                                .on('close.simplelightbox', function(){
                                    console.log('Requested for closing');
                                })
                                .on('closed.simplelightbox', function(){
                                    console.log('Closed');
                                    $('#overlay').fadeOut(200);
                                })
                        });
                    </script>


                </div>
            </div>
        </div>
        <?
    }
}


?>
