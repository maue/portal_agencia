<?

class artes_visuais {
    function render() {
		$largura = '515px';
?>

<div style="margin-bottom:0px; ">
  <div style=" position: relative; z-index: 9000; font-size:13px; font-family:Verdana, Geneva, sans-serif; "> 
    
    <!-- lateral direita cinza: -->
    
    <div style="background-color:#FF8122; color:#fff; position:relative; height:190px; padding-left:100px; padding-right:100px;" class="bloco_modalidade">
    <div class="titulo_modalidade"></div>
      <div class="texto_modalidade">
      <img src="img/laranja/logo_artes_visuais.jpg" style="position:absolute; left:60px;" />
      <div style="position:absolute; width:779px; left: 395px; top: 54px; height: 162px; text-align:justify">
        <p>Para termos uma visão de mundo crítica e reflexiva, precisamos de uma formação intelectual e artística. Por isso, valorizamos as Artes Visuais como disciplina, deixando de ser somente um momento de relaxamento para dar lugar aos estudos, aos experimentos, às produções e, principalmente, ser um espaço de criação.</p>
        
      </div>
      <div style="position:absolute; width:287px; left: 968px; top: 657px; height: 162px; text-align:justify; color:#000">Ao longo do ano letivo, são realizadas exposições nos corredores e no hall de entrada do Colégio, para que nossos artistas sintam a importância das suas produções.</div>
      </div>
    </div>
    <img src="img/artes_visuais.jpg" />
  </div>
</div>
<?
    }
}
?>
