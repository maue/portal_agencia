<?

class mobile_estrutura {
    function render() {
        ?>
<style>
    .container_miniatura {
        display: flex;
        /*flex-wrap:wrap;*/
        align-items: center;
        flex-direction: column;
        /*border:1px solid #ff0000;*/
    }
    .mob_miniatura {
        /*border:1px solid #ff0000;*/
        margin-bottom: 25px;
    }
    .mob_miniatura img {
        max-width: 95vw;
    }
    .mob_miniatura img.foto {
        border: 1px solid #fff;
    }
    div .link_nome_galeria {
        color: #fff;
    }
</style>
        <div class="bloco_modalidade mobile_pagina" style="background-color:#828D6B;">
            <div class="titulo_modalidade">CONHEÇA NOSSA ESTRUTURA</div>
            <div class="texto_modalidade">
                <div style="">

                    <?php


                    $c = EASYNC5__model_conn::get_conn();
                    $q = "SELECT 
            (
                SELECT id 
                FROM galeria_galeria 
                WHERE 
                    nome = 'Salas especiais' AND 
                    status = 3
            ) AS id_salas_especiais,
            (
                SELECT id 
                FROM galeria_galeria 
                WHERE 
                    nome = 'Recreação' AND 
                    status = 3
            ) AS id_recreacao,
            (
                SELECT id 
                FROM galeria_galeria 
                WHERE 
                    nome = 'Áreas Internas e Externas' AND 
                    status = 3
            ) AS id_areas_internas
            
            ";
                    $r = $c->qcv($q, "id_salas_especiais,id_recreacao,id_areas_internas");
                    $id_salas_especiais = (int)$r[0][0];
                    $id_recreacao = (int)$r[0][1];
                    $id_areas_internas = (int)$r[0][2];



                    $localh = $_SERVER['SERVER_NAME'] == 'localhost';
                    $relativo = '../novo/';
                    if($localh) {
                        $relativo = '../../portal/';

                    }


                    $nome_galeria = "Salas especiais";
                    $galeria_id = $id_salas_especiais;

                    if($localh) {
                        $galeria_id = 32;
                    }


                    $find_fotos = new EASYNC5___FIND__galeria_foto();
                    $find_fotos->filterByFk_galeria($galeria_id);
                    $find_fotos->orderBy(EASYNC5__galeria_foto::$COLUMN_nome);
                    $colecao = $find_fotos->get();





                    $caminho = '../../portal/arquivo_galeria/img_teste.jpg';
                    if(!$localh) {
                        $caminho = '../novo/arquivo_galeria/galeria_id00165-foto_id0000007636.jpg';
                    }



                    ?>
                    <div class="container_miniatura">
                        <?php

                        $galerias = array(
                            array("id" => $id_salas_especiais, "nome" => "Salas especiais", "arquivo_foto" => ""),
                            array("id" => $id_recreacao, "nome" => "Recreação", "arquivo_foto" => ""),
                            array("id" => $id_areas_internas, "nome" => "Áreas Internas e Externas", "arquivo_foto" => ""),
                        );

                        for($i=0; $i<sizeof($galerias); $i++) {
                            $id = $galerias[$i]["id"];
                            $q = "SELECT arquivo FROM galeria_foto WHERE fk_galeria = $id ORDER BY id LIMIT 1";
//                            echo $q;
                            $r = $c->qcv($q, "arquivo");
                            $galerias[$i]["arquivo_foto"] = $r[0];

                            if($localh) {
                                $galerias[$i]["arquivo_foto"] = 'img_teste.jpg';
                            }
                        }

//                        echo '<pre>';
//                        print_r($galerias);

                        for($i=0; $i<sizeof($galerias); $i++)
                        {
                            $galeria_id = $galerias[$i]['id'];
                            $arq = $galerias[$i]['arquivo_foto'];
                            $nome_galeria = $galerias[$i]['nome'];

                            $relativo = '../novo/';
                            if($localh) {
                                $relativo = '../../portal/';

                            }

                            echo '
<div class="mob_miniatura">
    <a class="link_nome_galeria" href="?acao=estrutura_fotos&galeria_id='. $galeria_id .'">
        <img class="foto" src="'.$relativo.'arquivo_galeria/'.$arq.'" />
        <div style="position: relative;">
            <div style="position: absolute; top:5px; left:30px;">'.$nome_galeria.'</div>
            <img src="img/rodape-galeria-estrutura-mobile.png" />
        </div>
    </a>
</div>
						';
                        }

                        ?>
                    </div>



                </div>
            </div>
        </div>

        <?
    }
}
?>
