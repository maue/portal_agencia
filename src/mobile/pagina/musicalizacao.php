<?

class musicalizacao {
    function render() {
		$largura = '515px';
?>

<div style="margin-bottom:0px; ">
  <div style=" position: relative; z-index: 9000; font-size:13px; font-family:Verdana, Geneva, sans-serif; "> 
    
    <!-- lateral direita cinza: -->
    
    <div style="background-color:#FF8122; color:#fff; position:relative; height:190px; padding-left:100px; padding-right:100px;" class="bloco_modalidade">
    <div class="titulo_modalidade"></div>
      <div class="texto_modalidade">
      <img src="img/laranja/logo_musicalizacao.jpg" style="position:absolute; left:60px;" />
      <div style="position:absolute; width:779px; left: 395px; top: 54px; height: 162px; text-align:justify">
        <p>As aulas de musicalização no Colégio PORTAL são realizadas uma vez por semana, do Maternal ao 9º ano.</p>
        <p> A iniciação musical com os alunos da Educação Infantil até 1º ano do Ensino Fundamental acontece por meio de atividades musicais que envolvem conceitos como som, ritmo, melodia e harmonia. Para que os alunos possam vivenciá-los, são utilizados instrumentos da bandinha rítmica e objetos alternativos construídos de sucata, brincadeiras cantadas, músicas folclóricas, de domínio popular e canções infantis de compositores da música popular brasileira. <br />
<br />

A partir do 2º ano, os alunos começam o estudo da Flauta Doce. 
Também passam a aprender a Teoria Musical para entenderem que
podem registrar os sons e ritmos que tocam. Assim seguem os demais anos do Ensino Fundamental I e II. Alguns alunos buscam o estudo de outros instrumentos como violão, piano, teclado e o canto coral. Surge então a possiblidade de trabalharmos as habilidades e interesses musicais de cada aluno. 
</p>
      </div>
      <div style="position:absolute; width:408px; left: 44px; top: 658px; height: 202px; text-align:justify"; >
          <div style="background-color:rgba(0, 0, 0, 0.5); padding:20px;   -webkit-border-radius: 10px;
-moz-border-radius: 10px;
border-radius: 10px; ">
        Com base em todo esse trabalho e motivados pelo recital "O Som do Portal Tim Tim Por Tim Tim", que acontece aos finais de ano, é que os alunos fazem uma linda apresentação musical.
          </div>
</div>


<div style="position:absolute; width:557px; left: 396px; top: 247px; height: 162px; text-align:justify;"></div>
      </div>
    </div>
    <img src="img/musicalizacao.jpg?random=123" />
  </div>
</div>
<?
    }
}
?>
