<?

class mobile_eventos {
    function render() {
        ?>
        
        <div class="bloco_modalidade mobile_pagina" style="background-color:#4E8B00;">
            <div class="titulo_modalidade">EDUCAÇÃO INFANTIL</div>
            <div class="texto_modalidade">
                <div style="">
            
                <div  style="margin-left:30px; margin-right:30px;">
                  <em>&quot;Conhecer o mundo em toda a sua diversidade de aspectos motiva a criança para aprender e possibilita sua inserção gradativa, consciente e ativa no meio em que vive...&quot; </em>(RCMEI)
    </div>
                <br>
                <br>
                Movidos por essa intenção, ampliando o mundo da criança de 2 a 5 anos, procuramos promover através de projetos de trabalho, inglês, música, artes visuais, natação e informática, atividades que estimulem suas capacidades para que possam desenvolver habilidades e competências que as auxiliem a enfrentar desafios.                 

                <br>
                <br>
                Valendo-se da ludicidade e utilizando uma metodologia interativa são ministradas três aulas semanais em Língua Inglesa, oportunizando aos alunos um contato mais frequente com a língua. 

            </div>
            </div>
        </div>
    <img src="img/educacao_infantil.jpg" />
        <?
    }
}
?>
