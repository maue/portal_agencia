<?

class metaplano {
    function render() {
		$largura = '515px';
?>

<div style="margin-bottom:0px; ">
  <div style=" position: relative; z-index: 9000; font-size:13px; font-family:Verdana, Geneva, sans-serif; "> 
    
    <!-- lateral direita cinza: -->
    
    <div style="background-color:#FF8122; color:#fff; position:relative; height:190px; padding-left:100px; padding-right:100px;" class="bloco_modalidade">
    <div class="titulo_modalidade"></div>
      <div class="texto_modalidade">
      <img src="img/laranja/logo_metaplano.jpg" style="position:absolute; left:60px;" />
      <div style="position:absolute; width:850px; left: 395px; top: 54px; height: 162px; text-align:justify">
        <p dir="ltr" id="docs-internal-guid-8123e9d8-7fff-6d77-eb88-8a28afdd4cf2">As atividades desenvolvidas através da Metodologia do Metaplano organizam o ensino de Matemática na construção dos conceitos de número inteiros e suas tabuadas, de números fracionários e das operações de adição, subtração, multiplicação e divisão desses campos numéricos. Essa metodologia inova na medida em que constroem os conceitos de fração e divisão a partir do 1º ano do Ensino Fundamental e permite também que uma tabuada não seja simplesmente decorada, favorecendo a prática de uma Matemática Reflexiva, proposta desta Instituição de Ensino. </p>
        
        <p><strong>Para saber mais</strong><br />

O Metaplano é um tabuleiro quadrado com 100 Pinos equidistantes entre
si e distribuídos simultaneamente em 10 linhas e 10 colunas. As atividades se
organizam com o cerco de Pinos utilizando "elásticos" em linhas ou colunas
individuais ou formando retângulos, quando utilizado mais de uma linha ou
coluna. A exploração da figura do retângulo é fundamental no desenvolvimento
da Metodologia.</p>
        
      </div>
      <div style="position:absolute; width:387px; left: 57px; top: 484px; height: 162px; text-align:justify; ">Nas atividades didáticas, as quantidades de Pinos cercados com os
elásticos possuem dois tratamentos distintos: Em um deles, formando ou não
retângulo, cada Pino representa uma variável discreta. Já no outro tratamento
considera-se o retângulo e os quadrados de menor área no seu interior, com
vértice em quatro Pinos. Esses quadrados de menor área, no interior dos
retângulos, são denominados de "quadradinhos" e representam variáveis
contínuas.</div>
      </div>
    </div>
    <img src="img/metaplano.jpg" />
  </div>
</div>
<?
    }
}
?>
