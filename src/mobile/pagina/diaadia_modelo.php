<?

class mobile_diaadia_modelo
{

    private $bg_color;
    private $img_slice;
    private $min_width;
    private $nome_pagina;
    private $img_rodape;
    private $img_aditional_params;
    private $em_construcao;
    private $max_width;

    public function __construct(
        $bg_color = '#FE8222',
        $img_slice = 'slice_r1_c1.jpg',
        $min_width = '150',
        $nome_pagina = 'PROJETOS',
        $img_rodape = 'projetos_retangular.jpg',
        $img_aditional_params = null,
        $em_construcao = false,
        $texto_em_construcao = '',
        $max_width = '200px'
    ) {
        $this->bg_color = $bg_color;
        $this->img_slice = $img_slice;
        $this->min_width = $min_width;
        $this->nome_pagina = strtoupper($nome_pagina);
        $this->img_rodape = $img_rodape;
        $this->img_aditional_params = $img_aditional_params !== null ? $img_aditional_params : '';
        $this->em_construcao = $em_construcao;
        $this->texto_em_construcao = $texto_em_construcao;
        $this->max_width = $max_width;
    }

    function render()
    {
?>

        <div class="bloco_modalidade mobile_pagina" style="background-color:<?= $this->bg_color; ?>;">

            <?
            if ($this->em_construcao) {
                echo '
                <div class="titulo_modalidade">'.$this->texto_em_construcao.'</div>
                <div class="texto_modalidade">Página em construção!</div>';
            } else {
            ?>
                <div style="display: flex; justify-content: center">
                    <img src="slices/<?= $this->img_slice; ?>" style="max-width:<?=$this->max_width;?>; margin-bottom:20px;" />
                </div>
                <div class="texto_modalidade">
                    <?

                    echo $this->em_construcao;
                    ?>
                    <div style="">



                        <?php



                        $array_id = config_site::get_id_paginas_inicio();
                        $id_pagina = $array_id[$this->nome_pagina];

                        $pagina = EASYNC5__site_inicio::getByPK($id_pagina);
                        $texto = $pagina->getTexto_mobile()->hasValue() ? $pagina->getTexto_mobile()->value() : '';
                        echo $texto;





                        ?>

                    </div>
                </div>

            <?
            }
            ?>
        </div>
        <?
        if (!$this->em_construcao) {
        ?>
            <img src="img/<?= $this->img_rodape; ?>?random=123" />
            <!--        <img src="img/ensino_fundamental1_retangular.jpg?random=123" />-->
        <?
        }
        ?>

<?
    }
}
?>