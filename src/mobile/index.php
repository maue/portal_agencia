<?

class mobile_index
{
    public function render()
    {
?>
        <div style="overflow-x: hidden; background-color: #F2F2F2">


            <!-- SMARTPHONE - INÍCIO -->
            <div class="d-lg-none">


                <nav class="navbar navbar-dark" style="background-color: #fff;">
                    <a class="navbar-brand" href="?acao=home">

                        <img src="img/LOGO.png" style="width: 135px; position: absolute; left: 20px; top: 10px" />
                    </a>

                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" style="background-color: #DA3523; color: #fff">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent" style="margin-top:20px;">
                        <ul class="navbar-nav mr-auto classe1">
                            <li class="nav-item">
                                <a class="nav-link" href="?acao=nossa_proposta">NOSSA PROPOSTA</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="?acao=estrutura_galeria">ESTRUTURA</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="?acao=matriculas">

                                    <?php
                                    // MATRÍCULAS
                                    $conn = EASYNC5__model_conn::get_conn();
                                    $q = "SELECT pagina FROM site_inicio WHERE id = 26;";
                                    $r = $conn->qcv($q, "pagina");
                                    $titulo = $r[0];
                                    echo $titulo;
                                    ?>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="?acao=contato">CONTATO</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="?acao=radar">RADAR</a>
                            </li>
                            
                            <li class="nav-item dropdown" style="margin:0; padding: 0">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    MAIS OPÇÕES
                                </a>

                                <?php


                                $localh = $_SERVER['SERVER_NAME'] == 'localhost';
                                $url_pai = 'http://www.colegioportalcuiaba.com.br/novo/sispai/index.php?acao=login';
                                $url_professor = 'http://www.colegioportalcuiaba.com.br/novo/sisaula/sistema/index.php?acao=login';
                                if ($localh) {
                                    $url_pai = '../novo/sispai/';
                                    $url_professor = '../novo/sisaula/';
                                }

                                ?>


                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="<?php echo $url_professor; ?>">PROFESSOR</a>
                                    <a class="dropdown-item" href="<?php echo $url_pai; ?>">PAI/ALUNO</a>
<!--                                    <a class="dropdown-item" href="?acao=material">MATERIAL</a>-->
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>


                <!-- CONTENT GOES HERE -->
                <?php


                $acoes_permitir_mobile = array(
                    'home',
                    'nossa_proposta',
                    'educacao_infantil',
                    'ensino_fundamental1',
                    'ensino_fundamental2'

                );

                if (isset($_GET['acao'])) {
                    //$acao = $_GET['acao'];
                    $acao = util::GET('acao');

                    if ($acao == 'home') {
                        $home = new mobile_home();
                        $home->render();
                    } else if ($acao == 'nossa_proposta') {
                        $home = new mobile_nossa_proposta();
                        $home->render();
                    } else if ($acao == 'educacao_infantil') {
                        $home = new mobile_educacao_infantil();
                        $home->render();
                    } else if ($acao == 'ensino_fundamental1') {
                        $home = new mobile_ensino_fundamental1();
                        $home->render();
                    } else if ($acao == 'ensino_fundamental2') {
                        $home = new mobile_ensino_fundamental2();
                        $home->render();
                    } else if ($acao == 'matriculas') {
                        $home = new mobile_matricula();
                        $home->render();
                    } else if ($acao == 'radar') {
                        $home = new mobile_radar();
                        $home->render();
                    } else if ($acao == 'contato') {
                        $home = new mobile_contato();
                        $home->render();
                    } else if ($acao == 'material') {
                        $home = new mobile_material();
                        $home->render();

                    } else if ($acao == 'projetos') {
                        // $home = new mobile_projetos();
                        // $home->render();

                        $home = new mobile_diaadia_modelo(
                            $bg_color = '#FE8222',
                            $img_slice = 'slice_r1_c1.jpg',
                            $min_width = '150',
                            $nome_pagina = 'PROJETOS',
                            $img_rodape = 'projetos_retangular.jpg',
                            $img_aditional_params = null
                        );
                        $home->render();
                        
                        
                    } else if ($acao == 'metaplano') {
                        // $home = new mobile_projetos();
                        // $home->render();

                        $home = new mobile_diaadia_modelo(
                            $bg_color = '#FE8222',
                            $img_slice = 'slice_r1_c2.jpg',
                            $min_width = '150',
                            $nome_pagina = 'METAPLANO',
                            $img_rodape = 'metaplano_retangular.jpg',
                            $img_aditional_params = null
                        );
                        $home->render();
                        
                        
                    } else if ($acao == 'artes_visuais') {
                        // $home = new mobile_projetos();
                        // $home->render();

                        $home = new mobile_diaadia_modelo(
                            $bg_color = '#FE8222',
                            $img_slice = 'slice_r1_c3.jpg',
                            $min_width = '150',
                            $nome_pagina = 'ARTES_VISUAIS',
                            $img_rodape = 'artes_visuais_retangular.jpg',
                            $img_aditional_params = null
                        );
                        $home->render();
                        
                        
                    } else if ($acao == 'natacao') {
                        // $home = new mobile_projetos();
                        // $home->render();

                        $home = new mobile_diaadia_modelo(
                            $bg_color = '#FE8222',
                            $img_slice = 'natacao_pagina.jpg',
                            $min_width = '150',
                            $nome_pagina = 'NATACAO',
                            $img_rodape = 'natacao_retangular.jpg',
                            $img_aditional_params = null
                        );
                        $home->render();
                        
                        
                        
                    } else if ($acao == 'musicalizacao') {
                        // $home = new mobile_projetos();
                        // $home->render();

                        $home = new mobile_diaadia_modelo(
                            $bg_color = '#FE8222',
                            $img_slice = 'musicalizacao_pagina.jpg',
                            $min_width = '150',
                            $nome_pagina = 'MUSICALIZACAO',
                            $img_rodape = 'musicalizacao_retangular.jpg',
                            $img_aditional_params = null
                        );
                        $home->render();
                        
                        
                        
                    } else if ($acao == 'eventos') {
                        // $home = new mobile_projetos();
                        // $home->render();

                        $home = new mobile_diaadia_modelo(
                            $bg_color = '#FE8222',
                            $img_slice = 'eventos_pagina.jpg',
                            $min_width = '150',
                            $nome_pagina = 'eventos',
                            $img_rodape = 'eventos_retangular.jpg',
                            $img_aditional_params = null
                        );
                        $home->render();
                        
                        
                        
                    } else if ($acao == 'atividades_extracurriculares') {
                        // $home = new mobile_projetos();
                        // $home->render();

                        $home = new mobile_diaadia_modelo(
                            $bg_color = '#FE8222',
                            $img_slice = 'atividades_extracurriculares_pagina.jpg',
                            $min_width = '150',
                            $nome_pagina = 'atividades_extracurriculares',
                            $img_rodape = 'atividades_extracurriculares_retangular.jpg',
                            $img_aditional_params = null
                        );
                        $home->render();
                        
                        
                        
                    } else if ($acao == 'alimentacao') {
                        // $home = new mobile_projetos();
                        // $home->render();

                        $home = new mobile_diaadia_modelo(
                            $bg_color = '#FE8222',
                            $img_slice = 'alimentacao_pagina.jpg',
                            $min_width = '150',
                            $nome_pagina = 'alimentacao_saudavel',
                            $img_rodape = 'alimentacao_retangular.jpg',
                            $img_aditional_params = null,
                            $em_construcao = false,
                            $texto_em_construcao = '',
                            $max_width = '150px'
                        );
                        $home->render();
                        
                        
                        
                        
                    } else if ($acao == 'estrutura_galeria') {
                        $home = new mobile_estrutura();
                        $home->render();
                    } else if ($acao == 'estrutura_fotos') {
                        $home = new mobile_estrutura_fotos();
                        $home->render(util::GET('galeria_id'));
                    }else {
                        $home = new mobile_home();
                        $home->render();
                    }
                } else {
                    $home = new mobile_home();
                    $home->render();
                }


                ?>


                <style>
                    .diaadia {
                        /*border:1px solid #000;*/
                    }

                    .diaadia .row .col div {
                        /*border-left:1px solid #0000ff;*/
                        margin: 0 auto;
                    }

                    .diaadia .row .col {
                        text-align: center;
                    }

                    .diaadia .row .col a div {
                        text-align: center;
                        width: 280px;
                        /*border:5px solid #eee;*/
                    }
                </style>


                <div style="
padding-top: 50px;
background-color: #FF8224;
">
                    <div class=" diaadia">
                        <div class="row no-gutters">
                            <div class="col">
                                <div style="font-family:'HelveticaLTStdRoman'; font-size: 24px; font-weight:bold; color: #ffffff; margin-bottom:20px;">
                                    DIA A DIA NO PORTAL
                                </div>
                            </div>
                        </div>
                        <div class="row no-gutters">
                            <div class="col">
                                <a href="?acao=projetos">
                                    <div style="height: 100%;"><img src="slices/slice_r1_c1.jpg" style="height: 141px; width: auto;" /></div>
                                </a>
                            </div>
                            <div class="col">


                                <a href="?acao=metaplano">
                                    <div style="height: 100%;"><img src="slices/slice_r1_c2.jpg" style="height: 141px; width: auto;" /></div>
                                </a>
                            </div>
                            <div class="col">

                                <a href="?acao=artes_visuais">
                                    <div style="height: 100%;"><img src="slices/slice_r1_c3.jpg" style="height: 141px; width: auto;" /></div>
                                </a>

                            </div>
                            <div class="col">

                                <a href="?acao=natacao">
                                    <div style="height: 100%;"><img src="slices/slice_r1_c4.jpg" style="height: 141px; width: auto;" /></div>
                                </a>

                            </div>
                            <div class="col">

                                <a href="?acao=musicalizacao">
                                    <div style="height: 100%;"><img src="slices/slice_r1_c5.jpg" style="height: 141px; width: auto;" /></div>
                                </a>

                            </div>
                            <div class="col">


                                <a href="?acao=eventos">
                                    <div style="height: 100%;"><img src="slices/slice_r1_c6.jpg" style="height: 141px; width: auto;" /></div>
                                </a>

                            </div>
                            <div class="col">

                                <a href="?acao=atividades_extracurriculares">
                                    <div style="height: 100%;"><img src="slices/slice_r1_c7.jpg" style="height: 141px; width: auto;" /></div>
                                </a>



                            </div>

                            <div class="col">


                                <a href="?acao=alimentacao">
                                    <div style="height: 100%;"><img src="slices/slice_r1_c8.jpg" style="height: 141px; width: auto;" /></div>
                                </a>

                            </div>
                        </div>
                    </div>
                </div>




                <DIV style="font-size:18px; font-weight:bold; color:#6F6F6F; font-family:'HelveticaLTStdCondensed'; padding-top:23px; float:left; margin:10px;">
                    Colégio Portal Cuiabá | Rua Alessandria, nº 05 - Jardim Itália - Cuiabá - MT, CEP: 78060-820 -
                    65 3664-2315 e 65 3664-3781
                </div>

                <div style="margin:0 auto; display:table;">
                    <a href="https://www.google.com/maps/place/Col%C3%A9gio+Portal/@-15.6053333,-56.0627656,15z/data=!4m5!3m4!1s0x0:0x42da8a7c73e81063!8m2!3d-15.60678!4d-56.058517">
                        <div style="background-color:#616161; color:#fff; padding:12px; padding-left:20px; padding-right:20px; font-size:15px;  font-family:Verdana, Geneva, sans-serif">
                            COMO CHEGAR
                        </div>
                    </a>
                </div>


                <div style="margin:0 auto; text-align:center; padding:20px; font-size:12px;"><a href="?ForcarLayoutDesktop=1">Forçar layout desktop</a></div>



                <?php

                $acao = 'home';
                if (isset($_GET['acao'])) {
                    //$acao = $_GET['acao'];
                    $acao = util::GET('acao');
                }
                $conn = EASYNC5__model_conn::get_conn();
                $q = "SELECT valor FROM si_parametro WHERE chave = 'EXIBIR_BANNER_PAGINA_INICIAL'";
                $r = $conn->qcv($q, "valor");
                $exibir = (int)$r[0];
                if($exibir == 1 && $acao == 'home') {
                    $rand = rand(100, 1000000);


                    $q = "SELECT valor FROM si_parametro WHERE chave = 'ARQUIVO_BANNER_PAGINA_INICIAL'";
                    $r = $conn->qcv($q, "valor");
                    $arq = $r[0]; // banner.jpg ou banner.png

                    $pasta = "arquivo_banner_pagina_inicial/$arq?rand=$rand";
                    $caminho = '../novo/' . $pasta;
                    if($_SERVER['SERVER_NAME'] == 'localhost') {
                        $caminho = '../../portal/' . $pasta;
                    }

                    ?>

                    <div id="div_banner_mobile" style="display: none; margin: 10px;">
                        <div class="link_fechar_banner" ><a href="javascript:fechar_banner_mobile();">Fechar</a></div>

                        <br>
                        <a href="javascript:fechar_banner_mobile();"><img style="width: 100%" src="<?=$caminho?>" /></a>
                    </div>
                    <?

                }




                ?>



            </div><!-- SMARTPHONE - FIM -->
        </div>

<?
    }
}

?>