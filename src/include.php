<?php

/*
 o caminho do site da agência será a site.com/site/
 por isso o caminho do sisaula volta na RAIZ do diretório,
 para incluir o que já está no ar da pasta 'novo' (que na verdade é o antigo sistema)
 
 */




$localh = $_SERVER['SERVER_NAME'] == 'localhost';
    
if($localh) {
    
    require_once "../../portal/sisaula/sistema/modelo/data.php";
    require_once "../../portal/sisaula/sistema/modelo/tabela.php";
    require_once "../../portal/sisaula/sistema/modelo/tabela_portal.php";
    require_once "../../portal/sisaula/sistema/modelo/entidade/conexao.php";
    require_once "../../portal/config_site.php";
    require_once "../../portal/sisaula/sistema/modelo/entidade/entities.php";
    require_once "../../portal/sisaula/sistema/modelo/entidade/easync5.php";
    include_once "../../portal/sisaula/sistema/modelo/util.php";

    require_once '../../portal/sisaula/sistema/modelo/sessao.php';
    require_once '../../portal/sisaula/sistema/controle/sessao.php';
    require_once '../../portal/sispai/modelo/sessao.php';
    
    
    
}else{
        
    require_once "../novo/sisaula/sistema/modelo/data.php";
    require_once "../novo/sisaula/sistema/modelo/tabela.php";
    require_once "../novo/sisaula/sistema/modelo/tabela_portal.php";
    require_once "../novo/sisaula/sistema/modelo/entidade/conexao.php";
    require_once "../novo/config_site.php";
    require_once "../novo/sisaula/sistema/modelo/entidade/entities.php";
    require_once "../novo/sisaula/sistema/modelo/entidade/easync5.php";
    include_once "../novo/sisaula/sistema/modelo/util.php";
    
    require_once '../novo/sisaula/sistema/modelo/sessao.php';
    require_once '../novo/sispai/modelo/sessao.php';
    
    
}



include_once 'pagina/home.php';
include_once 'pagina/reserva_de_matricula.php';
include_once 'pagina/educacao_infantil.php';
include_once 'pagina/ensino_fundamental1.php';
include_once 'pagina/ensino_fundamental2.php';
include_once 'pagina/em_construcao.php';


include_once 'pagina/diaadia_modelo.php';
include_once 'pagina/natacao.php';
include_once 'pagina/nossa_proposta.php';
include_once 'pagina/matriculas.php';
include_once 'pagina/matriculas_imagem.php';
include_once 'pagina/contato.php';
include_once 'pagina/musicalizacao.php';
include_once 'pagina/artes_visuais.php';
include_once 'pagina/metaplano.php';
include_once 'pagina/eventos.php';
include_once 'pagina/projetos.php';
include_once 'pagina/radar.php';
include_once 'pagina/atividades_extracurriculares.php';
include_once 'pagina/alimentacao.php';


include_once 'pagina/galeria_horizontal.php';
include_once 'pagina/estrutura_galeria.php';
include_once 'pagina/galerias.php';

include_once 'pagina/material.php';
include_once 'pagina/galeria_padrao.php';







include_once 'mobile/index.php';
include_once 'mobile/pagina/home.php';
include_once 'mobile/pagina/nossa_proposta.php';
include_once 'mobile/pagina/ensino_fundamental1.php';
include_once 'mobile/pagina/ensino_fundamental2.php';
include_once 'mobile/pagina/educacao_infantil.php';
include_once 'mobile/pagina/estrutura.php';
include_once 'mobile/pagina/estrutura_fotos.php';
include_once 'mobile/pagina/matricula.php';
include_once 'mobile/pagina/radar.php';
include_once 'mobile/pagina/contato.php';
include_once 'mobile/pagina/material.php';

// dia a dia
include_once 'mobile/pagina/diaadia_modelo.php';
include_once 'mobile/pagina/projetos.php';


?>