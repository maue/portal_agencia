<?php
error_reporting(E_ALL & ~(E_WARNING | E_NOTICE));
// error_reporting(E_ALL);
session_start();
include_once 'include.php';


if(util::GET_isset('acao')) {
    if(util::GET('acao') == 'pai_sair') {
        modelo__sessao_pai::encerrar_sessao();
        
        $host = $_SERVER['HTTP_HOST'];
        $site = '/novo/site';
        if($host == 'localhost') {
            $site = '/portal/site';
        }
        header('Location: http://' . $_SERVER['HTTP_HOST'] . $site);
    }
}


$randomURL = rand();


function setarCookie($key, $val) {
  setcookie($key, $val);
  $_COOKIE[$key] = $val;
}

$ForcarLayoutDesktopSim = "1";
$ForcarLayoutDesktopNao = "0";
$cookie = "ForcarLayoutDesktop";
// setcookie($cookie, $ForcarLayoutDesktop);


if(!isset($_COOKIE[$cookie])) {
  setarCookie($cookie, $ForcarLayoutDesktopNao);
}

if(util::GET($cookie) == $ForcarLayoutDesktopSim) {
  setarCookie($cookie, $ForcarLayoutDesktopSim);
}else{
  setarCookie($cookie, $ForcarLayoutDesktopNao);
}



?>
<!doctype html>
<html lang="en">
<head>



    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-147929366-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-147929366-1');
    </script>




    <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/styles.css?random=<?= $randomURL; ?>">

  <!-- Horizontal Estrutura -->
  <link rel="stylesheet" href="css/horizontal.css">

<?
if(util::GET('acao') == 'estrutura_galeria' || util::GET('acao') == 'galeria_padrao') {
?>
    <link href='https://fonts.googleapis.com/css?family=Slabo+27px' rel='stylesheet' type='text/css'>
    <link href='simplelightbox/dist/simplelightbox.css' rel='stylesheet' type='text/css'>
    <link href='simplelightbox/demo/demo.css' rel='stylesheet' type='text/css'>
<?
}
?>

  <script src="js/jquery.min.js"></script> 
  <script src="js/jquery.mask.js"></script> 
  <!-- <script src="js/popper.min.js"></script>  -->
  <script src="js/bootstrap.min.js"></script>
  <script type="text/javascript" src="simplelightbox/dist/simple-lightbox.js"></script>


  <title>Colégio Portal Cuiabá</title>
    <style>

        #div_banner {

            background-color: #fff;
            position: absolute; top:95px; z-index: 100001;

            -webkit-box-shadow: 5px 9px 38px 0px rgba(0,0,0,0.54);
            -moz-box-shadow: 5px 9px 38px 0px rgba(0,0,0,0.54);
            box-shadow: 5px 9px 38px 0px rgba(0,0,0,0.54);

            padding: 5px;
            border: 1px solid #999;

        }

        #div_banner_mobile {

            background-color: #fff;
            position: absolute; top:95px; z-index: 100001;

            -webkit-box-shadow: 5px 9px 38px 0px rgba(0,0,0,0.54);
            -moz-box-shadow: 5px 9px 38px 0px rgba(0,0,0,0.54);
            box-shadow: 5px 9px 38px 0px rgba(0,0,0,0.54);

            padding: 5px;
            border: 1px solid #999;

        }
        .link_fechar_banner {
            display: flex;
            justify-content: flex-end;
            font-size: 25px;
            margin: 10px;
        }
        .link_fechar_banner a {
        }
/*
      www.OnlineWebFonts.Com 
      You must credit the author Copy this link on your web 
      <div>Font made from <a href="http://www.onlinewebfonts.com">oNline Web Fonts</a>is licensed by CC BY 3.0</div>
      OR
      <a href="http://www.onlinewebfonts.com">oNline Web Fonts</a>
*/
/*@font-face {*/
    /*font-family: "Helvetica LT Std";*/
    /*src: url("http://db.onlinewebfonts.com/t/a0fd2131f7b17a68a0e7e3907bb9f58d.eot"); !* IE9*!*/
    /*src: url("http://db.onlinewebfonts.com/t/a0fd2131f7b17a68a0e7e3907bb9f58d.eot?#iefix") format("embedded-opentype"), !* IE6-IE8 *!*/
    /*url("http://db.onlinewebfonts.com/t/a0fd2131f7b17a68a0e7e3907bb9f58d.woff2") format("woff2"), !* chrome firefox *!*/
    /*url("http://db.onlinewebfonts.com/t/a0fd2131f7b17a68a0e7e3907bb9f58d.woff") format("woff"), !* chrome firefox *!*/
    /*url("http://db.onlinewebfonts.com/t/a0fd2131f7b17a68a0e7e3907bb9f58d.ttf") format("truetype"), !* chrome firefox opera Safari, Android, iOS 4.2+*!*/
    /*url("http://db.onlinewebfonts.com/t/a0fd2131f7b17a68a0e7e3907bb9f58d.svg#Helvetica LT Std") format("svg"); !* iOS 4.1- *!*/
/*}*/


@font-face {
    font-family: "Helvetica LT Std LIGHT";
    src: url("css/helvetica-lt-std-light.otf");
}



    .helvetica1 {
        font-family: "Helvetica LT Std LIGHT";
        /*font-weight:bold;*/
        font-size: 16px;
        color:#fff;
        font-weight: normal;
        line-height: 1.25;
        font-style: normal;
    }
    .helveticaItalico {
        font-family: "Helvetica LT Std LIGHT";
        /*font-weight:bold;*/
        font-style: italic;
        font-size: 16px;
        color:#fff;
        line-height: 1.25;
    }
    /* .texto1 { font-family: "Helvetica LT Std" } */



        .ano_galeria {
            background-color: #FFE4B6;
            padding: 8px;
            padding-left: 15px;
            padding-right: 15px;
            /*width: 200px;*/
            font-size: 16px;
            color: #333333;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            text-decoration: none;
            display: inline-block;
            margin: 5px;
        }

        .ano_galeria:hover {
            text-decoration: underline;
        }

        .ano_galeria:active {
            background-color: #DBDBDB;
            position: relative;
            top: 1px;
        }

        .classe1 a {
            color: #333 !important;
        }
    </style>
</head>

<body>



<!-- SMARTPHONE - INÍCIO -->
<?
$forcarDesktop = false;
if($_COOKIE[$cookie] == $ForcarLayoutDesktopSim) {
  $forcarDesktop = true;
}


// Temporário enquanto não está pronta a versão mobile:
//$forcarDesktop = true;

if(!$forcarDesktop) {
  $smartphone = new mobile_index();
  $smartphone->render();
}
?>
<!-- SMARTPHONE - FIM -->


<?php


$localh = $_SERVER['SERVER_NAME'] == 'localhost';
$testando_mobile = 0;

$exibir_desktop = true;
if($localh && $testando_mobile) {
    $exibir_desktop = false;
}

if($exibir_desktop) {

    ?>
    <!-- DESKTOP - INÍCIO -->
    <div class="<? if (!$forcarDesktop) {
        echo 'd-none d-lg-block';
    } ?>">
        <div class="main">
            <div class="frog0">
                <!-- FROG 0 -->
            </div>
            <div class="pond pond1">
                <!-- FROG 1 -->
            </div>
            <div class="pond pond2">

                <!-- abre largura1300 -->
                <div class="largura1300">
                    <!-- abre largura1300 -->
                    <!-- <div style="background-color:#D63422; text-align:center; color:#fff; font-size:15px; padding:10px; font-weight:bold">EM CONSTRUÇÃO</div> -->
                    <!-- <div class="d-none d-lg-block"> -->
                    <div>
                        <div style="width: 100%; text-align: center;  ">

                            <!-- <div style="background-image: url('img/bg_menu.jpg'); height: 80px; position: relative;"></div> -->
                            <div style="width: 1300px; display: inline-block; position: relative; text-align: left">
                                <div style="background-image: url('img/bg_menu.jpg'); height: 100px; position: relative;">
                                    <a href="?acao=home"> <img src="img/LOGO.png"
                                                               style="width: 195px; position: absolute; left: 20px; top: 20px"/>
                                    </a>
                                    <div class="unselectable linka" style="margin-left: 250px; font-size: 11px;
      white-space: nowrap; font-family:Verdana, Geneva, Tahoma, sans-serif"><a href="?acao=nossa_proposta">
                                            <div class="bordamenu"><span>NOSSA PROPOSTA</span></div>
                                        </a>

                                        <a href="?acao=estrutura_galeria">
                                            <!--                  <a href="?acao=galeria_padrao">-->
                                            <div class="bordamenu"><span>ESTRUTURA</span></div>
                                        </a>

                                        <a href="?acao=matriculas">
                                            <div class="bordamenu"><span>
                                                    <?php
                                                    //MATRÍCULAS 2021
                                                    $conn = EASYNC5__model_conn::get_conn();
                                                    $q = "SELECT pagina FROM site_inicio WHERE id = 26;";
                                                    $r = $conn->qcv($q, "pagina");
                                                    $titulo = $r[0];
                                                    echo $titulo;
                                                    ?>
                                                </span></div>
                                        </a> <a href="?acao=contato">
                                            <div class="bordamenu"><span>CONTATO</span></div>
                                        </a>
                                        <div id="acesso" style="display: inline-block; position: relative">
                                            <div class="bordamenu"><span>ACESSO PORTAL</span></div>
                                            <div id="opcoes_acesso"
                                                 style="display: block; position: absolute; right: 80px; top:65px; z-index: 100000">
                                                <?php


                                                $localh = $_SERVER['SERVER_NAME'] == 'localhost';
                                                $url_pai = 'http://www.colegioportalcuiaba.com.br/novo/sispai/';
                                                $url_professor = 'http://www.colegioportalcuiaba.com.br/novo/sisaula/sistema/';
                                                if ($localh) {
                                                    $url_pai = '../novo/sispai/';
                                                    $url_professor = '../novo/sisaula/';
                                                }

                                                ?>
                                                <a href="<?php echo $url_professor; ?>">
                                                    <div>Professor</div>
                                                </a>

                                                <a href="<?php echo $url_pai; ?>">
                                                    <div>Pai/Aluno</div>
                                                </a>

                                                <a href="?acao=material">
                                                    <div>Material</div>
                                                </a>
                                                <a href="?acao=galeria_padrao">
                                                    <div>Galeria de fotos</div>
                                                </a>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--

                        PAGE CONTENT

                    -->
                                <?


                                if (isset($_GET['acao'])) {
                                    //$acao = $_GET['acao'];
                                    $acao = util::GET('acao');

                                    if ($acao == 'home') {
                                        $home = new home();
                                        $home->render();
                                    } else if ($acao == 'reserva_de_matricula') {
                                        $home = new reserva_de_matricula();
                                        $home->render();
                                    } else if ($acao == 'educacao_infantil') {
                                        $home = new educacao_infantil();
                                        $home->render();
                                    } else if ($acao == 'ensino_fundamental1') {
                                        $home = new ensino_fundamental1();
                                        $home->render();

                                    } else if ($acao == 'ensino_fundamental2') {
                                        $home = new ensino_fundamental2();
                                        $home->render();

                                    } else if ($acao == 'natacao') {
                                        
                                        
                                        $home = new diaadia_modelo(
                                            $bg_color = '#FF8120',
                                            $img_slice = 'natacao_pagina.jpg',
                                            $min_width = '180',
                                            $nome_pagina = 'NATACAO',
                                            $img_rodape = 'natacao_retangular.jpg'
                                        );
                                        $home->render();


                                    } else if ($acao == 'em_construcao') {
                                        $home = new em_construcao();
                                        $home->render();

                                    } else if ($acao == 'estrutura_galeria') {
                                        $home = new estrutura_galeria();
                                        $home->render();

                                    } else if ($acao == 'galeria_padrao') {
                                        $home = new galeria_padrao();
                                        $home->render();


                                    } else if ($acao == 'nossa_proposta') {
                                        $home = new nossa_proposta();
                                        $home->render();

                                    } else if ($acao == 'musicalizacao') {
                                        
                                        
                                        $home = new diaadia_modelo(
                                            $bg_color = '#FF8120',
                                            $img_slice = 'musicalizacao_pagina.jpg',
                                            $min_width = '180',
                                            $nome_pagina = 'MUSICALIZACAO',
                                            $img_rodape = 'musicalizacao_retangular.jpg'
                                        );
                                        $home->render();



                                    } else if ($acao == 'artes_visuais') {
                                        // $home = new artes_visuais();
                                        // $home->render();

                                        
                                        $home = new diaadia_modelo(
                                            $bg_color = '#FF8120',
                                            $img_slice = 'artes_visuais_pagina.jpg',
                                            $min_width = '180',
                                            $nome_pagina = 'ARTES_VISUAIS',
                                            $img_rodape = 'artes_visuais_retangular.jpg'
                                        );
                                        $home->render();

                                    } else if ($acao == 'matriculas') {
                                        $home = new matriculas();
                                        $home->render();

                                    } else if ($acao == 'matriculas_imagem') {
                                        $home = new matriculas_imagem();
                                        $home->render();

                                    } else if ($acao == 'metaplano') {
//                                        $home = new metaplano();
//                                        $home->render();

                                        $home = new diaadia_modelo(
                                            $bg_color = '#FE8222',
                                            $img_slice = 'slice_r1_c2.jpg',
                                            $min_width = '180',
                                            $nome_pagina = 'METAPLANO',
                                            $img_rodape = 'metaplano_retangular.jpg'
                                        );
                                        $home->render();

                                    } else if ($acao == 'contato') {
                                        $home = new contato();
                                        $home->render();

                                    } else if ($acao == 'atividades_extracurriculares') {
                                        
                                        $home = new diaadia_modelo(
                                            $bg_color = '#FF8120',
                                            $img_slice = 'atividades_extracurriculares_pagina.jpg',
                                            $min_width = '140',
                                            $nome_pagina = 'ATIVIDADES_EXTRACURRICULARES',
                                            $img_rodape = 'atividades_extracurriculares_retangular.jpg',
                                            $img_aditional_params = ' max-width:200px; position:relative; left:-50px;'
                                        );
                                        $home->render();



                                    } else if ($acao == 'alimentacao') {
                                        
                                        $home = new diaadia_modelo(
                                            $bg_color = '#FF8120',
                                            $img_slice = 'alimentacao_pagina.jpg',
                                            $min_width = '140',
                                            $nome_pagina = 'ALIMENTACAO_SAUDAVEL',
                                            $img_rodape = 'alimentacao_retangular.jpg'
                                            ,$img_aditional_params = ' max-width:170px; position:relative; left:0px;'
                                        );
                                        $home->render();


                                    } else if ($acao == 'eventos') {
                                        
                                        $home = new diaadia_modelo(
                                            $bg_color = '#FF8120',
                                            $img_slice = 'eventos_pagina.jpg',
                                            $min_width = '140',
                                            $nome_pagina = 'EVENTOS',
                                            $img_rodape = 'eventos_retangular.jpg'
                                        );
                                        $home->render();



                                    } else if ($acao == 'projetos') {

                                        $home = new diaadia_modelo(
                                            $bg_color = '#FE8222',
                                            $img_slice = 'slice_r1_c1.jpg',
                                            $min_width = '150',
                                            $nome_pagina = 'PROJETOS',
                                            $img_rodape = 'projetos_retangular.jpg'
                                        );
                                        $home->render();

                                    } else if ($acao == 'radar') {
                                        $home = new radar();
                                        $home->render();

                                    } else if ($acao == 'material') {
                                        $home = new material();
                                        $home->render();

                                    } else {
                                        $home = new home();
                                        $home->render();
                                    }
                                } else {
                                    $home = new home();
                                    $home->render();
                                }

                                ?>
                                <div style="background-image:url(img/rodape-flat.png); padding-top:20px; ">
                                    <div style="font-family:'HelveticaLTStdRoman'; font-size: 24px; font-weight:bold; padding-left: 40px; color: #ffffff">
                                        DIA A DIA NO PORTAL
                                    </div>
                                    <div style="
margin-top: 17px;
background-color: #FF8224;
text-align: left;
z-index:100000;
">

                                        <div style="height: 181px; ">
                                            <div style="height: 181px;">
                                                <table border="0" cellspacing="0" cellpadding="0"
                                                       style="margin: 0px auto">
                                                    <tr>
                                                        <td><a href="?acao=projetos">
                                                                <div style="height: 100%;"><img
                                                                            src="slices/slice_r1_c1.jpg"
                                                                            style="height: 130px; width: auto;"/></div>
                                                            </a></td>
                                                        <td><a href="?acao=metaplano">
                                                                <div style="height: 100%;"><img
                                                                            src="slices/slice_r1_c2.jpg"
                                                                            style="height: 130px; width: auto;"/></div>
                                                            </a></td>
                                                        <td><a href="?acao=artes_visuais">
                                                                <div style="height: 100%;"><img
                                                                            src="slices/slice_r1_c3.jpg"
                                                                            style="height: 130px; width: auto;"/></div>
                                                            </a></td>
                                                        <td><a href="?acao=natacao">
                                                                <div style="height: 100%;"><img
                                                                            src="slices/slice_r1_c4.jpg"
                                                                            style="height: 130px; width: auto;"/></div>
                                                            </a></td>
                                                        <td><a href="?acao=musicalizacao">
                                                                <div style="height: 100%;"><img
                                                                            src="slices/slice_r1_c5.jpg"
                                                                            style="height: 130px; width: auto;"/></div>
                                                            </a></td>
                                                        <td><a href="?acao=eventos">
                                                                <div style="height: 100%;"><img
                                                                            src="slices/slice_r1_c6.jpg"
                                                                            style="height: 130px; width: auto;"/></div>
                                                            </a></td>
                                                        <td><a href="?acao=atividades_extracurriculares">
                                                                <div style="height: 100%;"><img
                                                                            src="slices/slice_r1_c7.jpg"
                                                                            style="height: 130px; width: auto;"/></div>
                                                            </a></td>
                                                        <td><a href="?acao=alimentacao">
                                                                <div style="height: 100%;"><img
                                                                            src="slices/slice_r1_c8.jpg"
                                                                            style="height: 130px; width: auto;"/></div>
                                                            </a></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div style="background-image:url(img/bg_rodape.jpg); height:92px; padding:15px;">


                                    <DIV style="text-align: center; font-size:18px; font-weight:bold; color:#6F6F6F; font-family:'HelveticaLTStdCondensed'; padding-top:7px; float:left; margin-left:100px;">
                                        Colégio Portal Cuiabá | Rua Alessandria, nº 05 - Jardim Itália - Cuiabá - MT,
                                        CEP: 78060-820 - 65 3664-2315 e 65 3664-3781<br><img src="img/insta.png"/>
                                        Instagram: <a class="link_insta"
                                                      href="https://www.instagram.com/colegio_portal/">@colegio_portal</a>
                                    </div>

                                    <div style="margin-top:20px; display:inline-block; float:right; margin-right:50px;">
                                        <a href="https://www.google.com/maps/place/Col%C3%A9gio+Portal/@-15.6053333,-56.0627656,15z/data=!4m5!3m4!1s0x0:0x42da8a7c73e81063!8m2!3d-15.60678!4d-56.058517">
                                            <div style="float:center; position:relative; top:-10px; background-color:#616161; color:#fff; padding:12px; padding-left:20px; padding-right:20px; font-size:15px; display:inline-block; font-family:Verdana, Geneva, sans-serif">
                                                COMO CHEGAR
                                            </div>
                                        </a>
                                    </div>
                                </div>

                                <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2711.127954066685!2d-56.05821728445118!3d-15.607055170818754!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x42da8a7c73e81063!2sCol%C3%A9gio+Portal!5e0!3m2!1spt-BR!2sbr!4v1481486638231"
                                                    width="1300" height="250" frameborder="0" style="border:0" allowfullscreen></iframe> -->
                            </div>
                        </div>
                    </div>

                    <?
                    if (util::GET('ForcarLayoutDesktop') == $ForcarLayoutDesktopSim) {
                        echo '<div style="margin:0 auto; text-align:center; padding-bottom:20px; font-size:12px;"><a href="?ForcarLayoutDesktop=0">Permitir layout mobile</a></div>      ';
                    }

                    ?>


                    <!-- fecha largura1300 -->
                </div>
                <!-- fecha largura1300 -->
            </div>
            <div class="pond pond3">
                <!-- FROG 3 -->
            </div>
            <div class="frog4">
                <!-- FROG 4 -->
            </div>


            <?php

            $acao = 'home';
            if (isset($_GET['acao'])) {
                //$acao = $_GET['acao'];
                $acao = util::GET('acao');
            }
            $conn = EASYNC5__model_conn::get_conn();
            $q = "SELECT valor FROM si_parametro WHERE chave = 'EXIBIR_BANNER_PAGINA_INICIAL'";
            $r = $conn->qcv($q, "valor");
            $exibir = (int)$r[0];
            if($exibir == 1 && $acao == 'home') {
                $rand = rand(100, 1000000);


                $q = "SELECT valor FROM si_parametro WHERE chave = 'ARQUIVO_BANNER_PAGINA_INICIAL'";
                $r = $conn->qcv($q, "valor");
                $arq = $r[0]; // banner.jpg ou banner.png

                $pasta = "arquivo_banner_pagina_inicial/$arq?rand=$rand";
                $caminho = '../novo/' . $pasta;
                if($_SERVER['SERVER_NAME'] == 'localhost') {
                    $caminho = '../../portal/' . $pasta;
                }

                ?>
                <div id="div_banner">
                    <div class="link_fechar_banner" ><a href="javascript:fechar_banner();">Fechar</a></div>

                    <br>
                    <a href="javascript:fechar_banner();"><img src="<?=$caminho?>" /></a>
                </div>
                <?

            }




            ?>

        </div>
    </div>

    <?php
}





?>
<!-- DESKTOP - FIM -->



<!-- Optional JavaScript --> 
<!-- jQuery first, then Popper.js, then Bootstrap JS --> 

<script>
$('#opcoes_acesso').hide();
$('#acesso').mouseover(function () {
    $('#opcoes_acesso').show();
});
$('#acesso').mouseout(function () {
    $('#opcoes_acesso').hide();
});

$('#carouselExampleSlidesOnly').carousel({
  interval: 3500
});

function fechar_banner() {
    $('#div_banner').hide();
}
function fechar_banner_mobile() {
    $('#div_banner_mobile').hide();
}


$(document).ready(function() {
    $('#div_banner').show();
    $('#div_banner_mobile').show();
});


function nextSlide() {
    jQuery('.slide.current').each(function() {
        var next = jQuery(this).next('.slide');
        if (!next.length) 
          next = jQuery(this).siblings('.slide').eq(0);
        jQuery(this).fadeOut().removeClass('current');
        next.fadeIn().addClass('current');
    }); 
    slidetimer=setTimeout('nextSlide()', slidespeed);
}


var slidespeed = 4000;
var slidetimer = null;
jQuery(document).ready(function() {
    // alert('iniciou 2');
    nextSlide();
});

</script>

</body>
</html>
