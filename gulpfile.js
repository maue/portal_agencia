/*
O  gulpfile.js está aqui somente para recompilar o bootstrap.css, 
que foi gerado com base no SASS 'node_modules/bootstrap/scss/bootstrap.scss'.

A alteração foi somente no line-height: 1.2;
E então foi executado o comando abaixo para regerar o .css baseado no .scss (penúltima linha):
    gulp.task('default', ['js', 'serve'])


*/
var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
const watchSass = require("gulp-watch-sass")


gulp.task('sass', function() {
    return gulp.src(['node_modules/bootstrap/scss/bootstrap.scss', 'src/scss/*.scss'])
    .pipe(sass())
    .pipe(gulp.dest('src/css'))
    .pipe(browserSync.stream());
});


gulp.task('js', function() {
    return gulp.src([
        'node_modules/bootstrap/dist/js/bootstrap.min.js', 
        'node_modules/jquery/dist/jquery.min.js', 
        'node_modules/popper.js/dist/umd/popper.min.js'])
        .pipe(gulp.dest("src/js"))
        .pipe(browserSync.stream());
});

gulp.task('serve', ['sass'], function() {
    browserSync.init({
        server: "./src",
        // port: 80
    });

    gulp.watch(['node_modules/bootstrap/scss/bootstrap.scss', 'src/scss/*.scss'], ['sass']);
    gulp.watch("src/*.html").on('change', browserSync.reload);
});

gulp.task("sass:watch", () => watchSass([
    "src/scss/*.scss"
  ])
    .pipe(sass())
    .pipe(gulp.dest("src/css")));

// gulp.task('default', ['js', 'serve'])
gulp.task('default', ['js', 'sass:watch'])